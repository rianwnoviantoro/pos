package com.example.pos254.pos.repositories;

import com.example.pos254.pos.models.OrderDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface OrderDetailRepo extends JpaRepository<OrderDetail, Long> {
    @Query("FROM OrderDetail WHERE OrderHeaderId = ?1")
    List<OrderDetail> FindByHeaderId(Long orderHeaderId);
}
